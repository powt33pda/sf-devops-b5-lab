# SF DevOps. Проектная работа №5

Задание:
- Создайте в Я.Облаке виртуальную машину под управлением Linux. Это будет ВМ для УЦ.
- Создайте новую внутреннюю доменную зону (используя Yandex Cloud DNS), затем добавьте в эту зону новую DNS-запись, указывающую на машину с Artifactory. Доменное имя используйте любое на свое усмотрение.
- На ВМ для УЦ создайте корневой и серверный сертификат для вашей машины (для выбранного вами доменного имени).
- Настройте Artifatory на использование SSL и созданного вами сертификата. (Выбирайте любой из вариантов: настройка SSL на TomCat, работающем с Artifactory веб-сервере, либо настройка связки nginx+Artifactory).
- Создайте еще одну машину в Я.Облаке, теперь под управлением Windows. Она потребуется для проверки выполнения задания.
- Установите созданный вами корневой сертификат на вашу ВМ под управлением Windows. (Созданное выше доменное имя будет работать только на машинах из Я.Облака, поэтому проверить настройки с помощью своего ноутбука не выйдет).
- С помощью веб-браузера зайдите (по HTTPS-протоколу) на машину, где развернут Artifactory, и убедитесь, что соединение защищено.

# Виртуальная машина для CA

## Конфигурация
Ресурсы
 - Платформа: Intel Ice Lake
 - Гарантированная доля vCPU: 50%
 - vCPU: 2
 - RAM: 2 ГБ
 - Объём дискового пространства: 50 ГБ

## Доступ 
 - IP: 51.250.12.160
 - Ключ: keys/private.ppk
 - Логин: pershin
 - Passpharse: см. менеджер паролей

## Установка nginx
```
sudo apt-get update
sudo apt-get install nginx
```

## Настройка nginx
Состоит из 2-х конфигов:

### nginx.conf
Здесь все стандартно, просто убеждаемся, что SSL включен:
```
ssl_protocols TLSv1 TLSv1.1 TLSv1.2 TLSv1.3; # Dropping SSLv3, ref: POODLE
ssl_prefer_server_ciphers on;
```

### conf.d/artifactory
Кастомный конфиг для нашего сайта. Важные моменты:

``` 
# Указываем путь до нашего сертификата
ssl_certificate      /etc/ssl/certs/domain.crt;
ssl_certificate_key  /etc/ssl/private/domain.key;

# Указываем доменное имя, которые мы присвоили серверу
server_name artifactory.sf-devops-33pda.com;
```

```
 # IP адрес нашего сервера Artifactory
 proxy_pass          http://51.250.0.107:8082; 
```

# Доменное имя
Настраиваем в Yandex Cloud DNS. Создаем новую DNS зону, затем внутри создаем новую DNS запись и присваиваем ее нашему nginx/CA серверу нужное имя, которое мы будем потом указывать в сертификате и в конфиге nginx. У меня это artifactory.sf-devops-33pda.com.

## Сертификат

### Создание
Необходимо создать самоподписанный сертификат, делается это командой:
```
openssl req -newkey rsa:2048 -nodes -keyout domain.key -x509 -days 365 -out domain.crt -config req.cnf
```
для выполнения которой необходимо создать и заполнить файл req.cnf
```
[req]
distinguished_name = req_distinguished_name
x509_extensions = v3_req
prompt = no
[req_distinguished_name]
C = RT
ST = Altay State
L = Barnaul
O = Skill Factory
OU = DevOps-16
CN = artifactory.sf-devops-33pda.com
[v3_req]
keyUsage = critical, digitalSignature, keyAgreement
extendedKeyUsage = serverAuth
subjectAltName = @alt_names
[alt_names]
DNS.1 = artifactory.sf-devops-33pda.com
```
CN - то самое доменное имя NGINX.

### Размещение
Полученные файлы domain.key и domain.crt необходимо подложить на сервер nginx:
```
sudo cp ~/domain.key /etc/ssl/private/domain.key
sudo cp ~/domain.crt /etc/ssl/certs/domain.crt
```

# Проверка
Необходимо:
1. Поднять новую VM на Windows в Я.Облаке
2. Установить в системе сертификат domain.crt как доверенный
3. В браузере проверить, что соединение безопасно
